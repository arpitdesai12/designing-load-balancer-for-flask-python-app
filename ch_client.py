import bisect
import md5
import urllib2
import json
import requests
class ConsistentHashRing():

    def __init__(self):
        self._keys = []
        self._nodes = {}

    def _hash(self, key):
        return long(md5.md5(key).hexdigest(), 16)

    def __setitem__(self, nodename, node):
        hash_ = self._hash(str(nodename))
        if str(hash_) in self._nodes:
            raise ValueError("Node name %r is already present" % nodename)
        else:
            self._nodes[hash_] = node
            bisect.insort(self._keys, hash_)

    def __delitem__(self, nodename):
        hash_=self._hash(str(nodename))
        if str(hash_) in self._nodes:
            del self._nodes[hash_]
            index = bisect.bisect_left(self._keys, hash_)
            del self._keys[index]
        else:
            raise ValueError("Node name %r is "
                            "not present" % nodename)
    
    def __getitem__(self, key):
        hash_ = self._hash(key)
        start = bisect.bisect(self._keys, hash_)
        if start == len(self._keys):
            start = 0
        return self._nodes[self._keys[start]]


def main():
    cr = ConsistentHashRing()
    nodes=["127.0.0.1:5000", "127.0.0.1:5001", "127.0.0.1:5002"]
    cr["node1"]=nodes[0]
    cr["node2"]=nodes[1]
    cr["node3"]=nodes[2]

    for i in range(1,11):
        url="http://"+cr[str(i)]+"/v1/expenses/"+str(i)
        data = {    'id' : str(i),
                'name' : 'Foo'+str(i),
                'email' : 'foo1@bar.com',
                'category': 'office supplies',
                'description' : 'iPad for office use',
                'link' : 'http://www.apple.com/shop/buy-ipad/ipad-pro',
                'estimated_costs' : '700',
                'submit_date' : '12-10-2016'}
    
        req = urllib2.Request(url)
        req.add_header('Content-Type', 'application/json')
        response = urllib2.urlopen(req, json.dumps(data))
        
    for i in range(1,11):
        url="http://"+cr[str(i)]+"/v1/expenses/"+str(i)
        response = requests.get(url)
        data = response.json() 
        print url
        print data       

if  __name__ =='__main__':
    main()